function getCube(num){
const cube=num**3
console.log(`The cube of ${num} is ${cube}`)
}


const address=[258,"Washington Ave.","NW","California",90011]
const [number,street,city,state,code]=address;
console.log(`I live ate ${number} ${street} ${city}, ${state} ${code}`);


let animal = {
	name:"Lolong",
	kind: "saltwater crocodile",
	weight: 1075,
	feet: 20,
	inches: 3
}

const {name,kind,weight,feet,inches} = animal;
console.log(`${name} was a ${kind}. He weighed ${weight} kgs with a measurement of ${feet} ft ${inches} in.`);


const nums=[2,5,14,24,45]
nums.forEach(x=>console.log(x))
const reduceNumber=nums.reduce((a,b)=>a+b)
console.log(reduceNumber)


class Dog{
	
	constructor (name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
};

const dog1 = new Dog("Just", 6, "Border Collie");
console.log(dog1);
