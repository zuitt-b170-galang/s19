// JS ES6 updates
//ECMAScript - the technology that is used to create the languages such as JavaScript

// exponent operator
	// pre-es6
	const firstNum = Math.pow(8, 2);
	console.log(firstNum);

	// es6
	const secondNum = 8**2;
	console.log(secondNum);


// Template literals - allows writing of strings without the use of concat operator; helps in terms of readability of the codes as well as the efficiency of work.
/*
-create a name variable and store a name of a person
-create a message variable, greeting the person and welcoming him/her in the programming field
log in the console the message
*/
// pre-es6
let name = "John";
let message = "Hello " + name + "! Welcome to the programming field";
console.log (message);

// es6
message = `Hello ${name}! Welcome to the programming field`;
console.log (message);

// Mulitine
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8**2 with the answer of ${secondNum}
`
console.log(anotherMessage);

// computation inside the template literals
const interestRate = .10;
const principal = 1000;
console.log(`The interest on your savings is ${principal * interestRate}`);


// Array Destructuring - allows unpacking elements in arrays into distinct variables; allows naming of array elements with variable instead of using index numbers; helps with the code readability and coding efficiency
/*
	SYNTAX:
		let/const [ variableA, variableB, variableC, ... variableN ] = arrayName
*/
/*
create an array that has firstName,  middleName, and lastName elements
log in the console each element (1 row - 1 element)
*/
const fullName = [ "Juan", "Dela", "Cruz" ];
// pre-es6
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}!`);

// es6
const [ firstName, middleName, lastName ] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}!`);


// Object Destructuring - allows unpacking elements in object into distinct variables; shortens the syntax for accesssing the objects
/*
create an object that contains a woman's givenName, maidenName, familyName
log in the console each key (1 row - 1 key-value pair)
*/
let woman = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}
// pre-es6
console.log(woman.givenName);
console.log(woman.maidenName);
console.log(woman.familyName);
console.log(`Hello ${woman.givenName} ${woman.maidenName} ${woman.familyName}!`);

// es6
// the key and the variable should match
const {givenName, maidenName, familyName} = woman;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);

// Arrow Function
/*
	compact alternative syntax to traditional functions; useful for code snippets where creating functions will be reused in any other parts of the code; Don't Repeat Yourself - there is no need to create a function that will not be used in the other parts/portions of the code
*/
/*
-create a function that displays/logs a person's firstName, middleInitial, and lastName in the console.
*/
// pre-es6
/*
Parts
	declaration
	functionName
	parameters
	statements
	invoke/call the function
*/
function printFullName(firstName, middleInitial, lastName){
/*	console.log(firstName);
	console.log(middleInitial);
	console.log(lastName);*/
	console.log(firstName, middleInitial, lastName);
};

printFullName("Jane","R.","Rivera");

// es6
const printFName=(fname, mname, lname) =>{
	console.log(fname, mname, lname);
}
printFName("Will", "D.", "Smith");

// arrows functions with loop/methods
/*
	-use forEach method to log each student in the console.
*/
const students = ['John', 'Jane', 'Joe'];
// pre-es6
students.forEach(
    function(pick){
        console.log(pick);
    }
);
// es6
	// if you have 2 or more parameters, enclose them inside a pair of parenthesis
students.forEach(x => console.log(x));

// (Arrow Functions) Implicit Return Statement - return statement/s are omitted because even without them, JS implicitly add them for the result of the function
/*
 - create a function that adds two numbers using return statement
*/
// pre-es6
function addNumbers(x, y){
	return x + y
};
let total = addNumbers(1, 2);
console.log(total);

// es6
const adds = (x, y) => x + y;
// the code above actually runs as const adds = (x, y) => return x + y;
let totals = adds(4, 6);
console.log(totals);

// (Arrow Function) Default Function Argument Value
/*
	provides a default argument value if no parameters are included/specified once the function has been invoked
*/

const greet = (name = "User") => {
	return `Good morning, ${name}`
}
console.log(greet());
// once the function has specified parameter value
console.log(`Result of specified value for paramter: ${greet("John")}`);

/*
 - create a car object using the object method
 	car field: name, brand, year 	
*/
/*function car(name, brand, year){
	this.name = name
	this.brand = brand
	this.year = year
};

const car1 = new car("Honda", "Vios", 2020);
console.log(car1)*/

// CLASS CONSTRUCTOR
// class keyword declares the creation of a "car" object
class car{
	// constructor keyword - special method of creating/initializing an object for the "car" class
	constructor (brand, name, year){
		// this - sets the properties that are included to the "car" class
		this.brand = brand,
		this.name = name,
		this.year = year
	}
};
const car1 = new car("Ford", "Ranger Raptor", 2021);
console.log(car1);

const car2 = new car();
console.log(car2);
car2.brand = "Toyota",
car2.name = "Fortuner",
car2.name = 2020
console.log(car2);

/*
- create a variable that stores a number
 - create an if-else statement using ternary operator
 	condition = if the number is <=0, return true, if it is >0, return false
*/
let num = 10;

(num <= 0 ) ? console.log(true) : console.log(false);

/*if (num <= 0 ){
	console.log(true)
}else{
	console.log(false)
}*/
